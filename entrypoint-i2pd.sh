#!/bin/sh

i2pd_files_ready() {
    [ -s /home/i2pd/data/router.info ] && \
      [ "$(ls -1 /home/i2pd/data/destinations | grep -c '.dat$')" -ge 1 ] && \
      [ -s "$(printf '%s' /home/i2pd/data/destinations/*.dat)" ]
}

if [ -n "${RESEED_WAIT_URL:-}" ]; then

    # start the daemon and kill it after it's created it's initial router.info file (veeery ugly hack :-///)
    /home/i2pd/bin/i2pd --datadir=/home/i2pd/data --conf=/home/i2pd/conf/i2pd.conf &
    i2pdpid=$!

    # wait for all the neccessary files to be created,
    # for bootstrapping and testing net network

    while ! i2pd_files_ready; do
            sleep 1
    done
    sync # make sure the files are fully written
    kill "$i2pdpid"


    # in the background the router infos should be copied over
    # to the reseed server, if it has enough it will become available
    while ! curl -s "$RESEED_WAIT_URL"; do
        echo "Waiting for reseeder at $RESEED_WAIT_URL to become available..."
        sleep 1
    done
fi

exec /home/i2pd/bin/i2pd --datadir=/home/i2pd/data --conf=/home/i2pd/conf/i2pd.conf
